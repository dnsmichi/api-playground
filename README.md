# API Playground

> **Note**: This project is not actively maintained anymore.
>
> The Python code snippets have been migrated into the [GitLab Developer Evangelism project](https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-python), and refreshed for Python 3.8+ support.

GitLab API playground with code samples for various languages.

- [python/](python/README.md)
- [perl/](perl/README.md)

