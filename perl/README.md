# GitLab Perl API Client Playground

## Installation

```
perl -MCPAN -e 'install B::Hooks::EndOfScope'
perl -MCPAN -e 'install IO::Socket::SSL'
perl -MCPAN -e 'install GitLab::API::v4'
```

```
export GITLAB_COM_PRIVATE_TOKEN=xxx
```

## Queries

Modify `query.pl` and/or run it.
