#!/usr/bin/env python

import gitlab
import os

# Goal: Try to download README.md from https://gitlab.com/everyonecancontribute/general
FILE_NAME = 'README.md'
BRANCH_NAME = 'main'

# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#getting-started-with-the-api
SERVER='https://gitlab.com'
GROUP_NAME='everyonecancontribute'
PROJECT_NAME='general'

# Prefer keyset pagination
# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination
#gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'], pagination="keyset", order_by="id", per_page=100)
gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'])

project = gl.projects.get(GROUP_NAME + "/" + PROJECT_NAME)

# Print & simulate search over all files - override FILE_NAME in case
for f in project.repository_tree():
    print("File path '%s' with id '%s'" % (f['name'], f['id']))

    if f['name'] == FILE_NAME:
        f_content = project.repository_raw_blob(f['id'])
        print(f_content)

# Alternative approach: Get the file from the main branch
raw_content = project.files.raw(file_path=FILE_NAME, ref=BRANCH_NAME)
print(raw_content)

# Store the file on disk
with open('raw_README.md', 'wb') as f:
    project.files.raw(file_path=FILE_NAME, ref=BRANCH_NAME, streamed=True, action=f.write)




