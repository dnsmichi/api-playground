# GitLab Python API Client Playground

> **Note**: This project is not actively maintained anymore.
>
> The code snippets have been migrated into the [GitLab Developer Evangelism project](https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-python), and refreshed for Python 3.8+ support.

## Installation

```
pip install -r requirements.txt
```

## Configuration

Examples use a configuration file, or the exported `GITLAB_TOKEN` environment variable.

```
vim $HOME/.python-gitlab.cfg

[global]
default = gitlab.com

[gitlab.com]
url = https://gitlab.com
private_token = xxx
api_version = 4
ssl_verify = true
```

## Use Cases

### Search file path in Merge Requests

During the [re-organisation of the GitLab marketing handbook](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/13991), a huge MR was created which potentially fails with other open MRs in the repository that modify the same (renamed) file paths.

The script [search_mr_contains_path.py](search_mr_contains_path.py) does the following:

- Get all MRs from a project, https://gitlab.com/gitlab-com/www-gitlab-com as default `PROJECT_ID`
- Extract the MR Diff via API, and look into `old_path` which is the file path location we want to match against
- Check the path against the defined array of paths to be matched
- Log the match, and store the MR for later summary processing
- Include MR merge status, title, URLs to support copy/past into Markdown issues
- Optional: Ignore MRs that prepare the changes and would cause false positives

Edit [search_mr_contains_path.py](search_mr_contains_path.py) and adjust the `PROJECT_ID`, `PATH_PATTERNS` and `IGNORE_MRS` variables.

```
export GITLAB_TOKEN=xxx
python3 search_mr_contains_path.py
```

### Issue Index Creation

Edit [gen_issue_index.py](gen_issue_index.py) and modify `PROJECT_ID`, `FILE_NAME` and `REF_NAME` for your likings.

```
export GITLAB_TOKEN=xxx
python3 gen_issue_index.py
```

Can be added as [GitLab CI/CD Schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).

```
gen-issue-index:
  image: python:3.10
  script:
    - pip install -r python/requirements.txt
    - python gen_issue_index.py
```

### Snippets Index by Due Date

- Parses a directory tree with markdown files (naming is not important).
- Orders entries by due date, and prints a Markdown overview ready for index.md
- Uploads index.md in a given path into the GitLab repository.


#### Parsed Format

- Heading 1 title
- Footer
  - `Tags` list
  - `Due` date parse, YYYY-MM-DD expected

```
# 2022-07-19 Notes

HN topic about taking notes: https://news.ycombinator.com/item?id=32152935

<!--
---
Tags: DevOps, Learn
Due: 2022-08-01
---
-->
```

#### Usage

Edit [gen_snippets_index_by_due_date.py](gen_snippets_index_by_due_date.py) and modify `PROJECT_ID`, `DIR_NAME`, `INDEX_PATH` and `REF_NAME` for your likings.

```
export GITLAB_TOKEN=xxx
python3 gen_snippets_index_by_due_date.py
```

#### Demo

Demo is sourced from [demo/snippets](../demo/snippets) and generates an [index.md](../demo/snippets/index.md) overview.

```
# Snippets Overview
_Ordered by due date._


## 2022-07-22

- [2022-07-21 Notes](2022-07-21.md) (Release Upgrade)

## 2022-08-01

- [2022-07-19 Notes](2022-07-19.md) (DevOps Learn)

## 2022-09-01

- [2022-07-20 Notes](2022-07-20.md) (SRE Observability Talk)
```

#### Automation

Can be added as [GitLab CI/CD Schedule](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).

```
gen-snippets-by-due-date-index:
  image: python:3.10
  script:
    - pip install -r python/requirements.txt
    - python gen_snippets_index_by_due_date.py
```



## Examples

### Queries

Modify [query.py](query.py) and/or run it.

### Pagination

Explore [pagination.py](pagination.py) for how it works.

### Topics

Query projects and match topics in [topics.py](topics.py).

### Raw File from Project

Download a raw file from a GitLab project in [raw_file.py](raw_file.py).

### User search by pattern

Search for user names containing a specific pattern in [user_search.py](user_search.py).


### Errors

Generate an unauthenticated error and more in [error.py](error.py)

### List K8s Clusters

(Certificate based that are deprecated)

The script starts at the group level and looks for group clusters, and all sub groups and projects clusters in [list_k8s_clusters.py](list_k8s_clusters.py) with a `GROUP_ID` env variable. `GITLAB_TOKEN` requires at least maintainer permissions for all groups and projects.


