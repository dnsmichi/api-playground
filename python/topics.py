#!/usr/bin/env python

import gitlab
import os

# Goal: Try to query https://gitlab.com/everyonecancontribute/general as a match
TOPICS = [ 'everyonecancontribute', 'community' ]

# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#getting-started-with-the-api
SERVER='https://gitlab.com'
GROUP_NAME='everyonecancontribute'

# Prefer keyset pagination
# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination
gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'], pagination="keyset", order_by="id", per_page=100)

projects = []

# NOTE: Iterating public projects on GitLab.com is expensive. We limit the scope to a specific group
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html
groups = gl.groups.list(as_list=False)

for group in groups:
    if GROUP_NAME in group.name:
        projects = group.projects.list()

# TODO: subgroups from the group, see https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#subgroups 

# For a self-managed instance, use the following snippet
# projects = gl.projects.list(all=True)

# Check all projects' topics attributes
print ("Hello from %s %s - checking against project topics %s" %('GitLab', '🦊', ','.join(TOPICS)))

for p in projects:
    if len(p.topics) > 0:
        for t in p.topics:
            if t in TOPICS:
                print("MATCH: Topic %s on project %s" % (t, p.name))



