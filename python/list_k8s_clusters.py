#!/usr/bin/env python

import gitlab
import os
import sys

def print_clusters(clusters):
    for c in clusters:
        print(c)
        print("\n\n")

GITLAB_SERVER = os.environ.get('GITLAB_SERVER', 'https://gitlab.com')
# https://gitlab.com/everyonecancontribute
GROUP_ID = os.environ.get('GROUP_ID', 8034603)
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GITLAB_TOKEN env variable.")
    sys.exit(1)

# TOKEN requires maintainer+ permissions

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# All groups on gitlab.com takes too long; scope it down to your group namespace via GROUP_ID. For self-managed, try experimenting with the line below to fetch all groups.
#groups = gl.groups.list(get_all=True)
groups = [ ]

# get GROUP_ID group
groups.append(gl.groups.get(GROUP_ID))

for group in groups:
    for sg in group.subgroups.list(include_subgroups=True, all=True):
        real_group = gl.groups.get(sg.id)
        groups.append(real_group)

group_clusters = {}
project_clusters = {}

for group in groups:
    #Collect group clusters
    g_clusters = group.clusters.list()

    if len(g_clusters) > 0:
        group_clusters[group.id] = g_clusters

    # Collect all projects in group and subgroups and their clusters
    projects = group.projects.list(include_subgroups=True, all=True)

    for project in projects:
        # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
        manageable_project = gl.projects.get(project.id)

        # skip archived projects
        if project.archived:
            continue

        p_clusters = manageable_project.clusters.list()

        if len(p_clusters) > 0:
            project_clusters[project.id] = p_clusters

# Print summary
print("## Group clusters\n\n")

for g_id, g_clusters in group_clusters.items():
    url = gl.groups.get(g_id).web_url

    print("Group ID {g_id}: {u}\n\n".format(g_id=g_id, u=url))

    print_clusters(g_clusters)


print("## Project clusters\n\n")

for p_id, p_clusters in project_clusters.items():
    url = gl.projects.get(p_id).web_url

    print("Project ID {p_id}: {u}\n\n".format(p_id=p_id, u=url))

    print_clusters(p_clusters)




