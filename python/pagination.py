#!/usr/bin/env python

import gitlab
import os

# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#getting-started-with-the-api
SERVER='https://gitlab.com'
GROUP_NAME='everyonecancontribute'

# Prefer keyset pagination
# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination
gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'], pagination="keyset", order_by="id", per_page=100)

# Iterate over the list, and fire new API calls in case the result set does not match yet
groups = gl.groups.list(as_list=False)

found_page = 0

for group in groups:
    if GROUP_NAME in group.name:
        print(group.attributes)
        found_page = groups.current_page
        break

print("Pagination API example for Python with %s %s - result on page %d" % ("GitLab", "🦊", found_page))


