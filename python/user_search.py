#!/usr/bin/env python

import gitlab
import os
import re

# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#getting-started-with-the-api
SERVER='https://gitlab.com'
SEARCH_PATTERN=r"\."

# Prefer keyset pagination
# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination
gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'], pagination="keyset", order_by="id", per_page=100)

# Iterate over the list, and fire new API calls in case the result set does not match yet
users = gl.users.list(as_list=False)

found_users_count = 0
found_users = []

for user in users:
    search_return = re.search(SEARCH_PATTERN, user.username)

    if search_return:
        found_users_count += 1
        found_users.append(user.username)
        print("Found user name '%s', overall count: %d" % (user.username, found_users_count))


print("GitLab API: Found %d users with '%s': %s" % (found_users_count, SEARCH_PATTERN, ",".join()))


