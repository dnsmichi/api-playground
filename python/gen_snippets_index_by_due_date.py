#!/usr/bin/env python

import gitlab
import os
#import sys
import re
from datetime import date

SERVER='https://gitlab.com'
PROJECT_ID=17255375 # https://gitlab.com/dnsmichi/api-playground
DIR_NAME='demo/snippets'
INDEX_PATH=DIR_NAME+'/index.md'
REF_NAME='main'

# Parse footer
## 2022-07-19 Notes
#
#HN topic about taking notes: https://news.ycombinator.com/item?id=32152935
#
#<!--
#---
#Tags: DevOps, Learn
#Due: 2022-08-01
#---
#-->
def parse_footer(content):
    meta = {}
	# Note: Could be one regex with 3 groups, splitting for readability

	# title
    m = re.search('^#\s(.*)\\n', content)
    if m:
        meta['title'] = m.group(1)

	# Tags
    m = re.search('Tags:(.*)\\n', content)
    if m:
        meta['tags'] = [x.strip() for x in m.group(1).split(',')] # create a list of tags

	# Due
    m = re.search('Due:(.*)\\n', content)
    if m:
        meta['due'] = m.group(1).strip()

    # DEBUG
    #print(meta)

    return meta


# Main
gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'])

# https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html
p = gl.projects.get(PROJECT_ID)

index = {}

items = p.repository_tree(path=DIR_NAME, ref=REF_NAME)

for c in range(len(items)):
    # Use the file path, instead of repository_blob
    path = items[c]['path']

    if INDEX_PATH in path:
        continue

    f = p.files.get(file_path=path, ref=REF_NAME)

    # Note: decode() is a python-gitlab object type function for files.
    # Calling .decode('utf-8') is required to convert bytes to string.
    content = f.decode().decode('utf-8')

    # DEBUG
    #print(path)
    #print(content)

    # Extract the footer tags and due date.
    meta = parse_footer(content)

    # Add file path to meta information
    meta['file_path'] = path.replace(DIR_NAME+'/', '')

    # DEBUG
    #print(meta)

    # Prepare for grouping
    index[meta['due']] = meta

index_str = """# Snippets Overview
_Ordered by due date._

"""

# DEBUG
#print(index)

for key_due, meta in sorted(index.items()):
    index_str += "\n## {due} \n\n".format(due=key_due)

    #print(key_due)
    #print(meta)

    index_str += "- [{title}]({path}) ({tags})\n".format(title=meta['title'], path=meta['file_path'], tags=" ".join(meta['tags']))


# DEBUG
print(index_str)

# DEBUG: STOP
#sys.exit(0)

# Dump index_str to FILE_NAME
# Create as new commit
# See https://docs.gitlab.com/ce/api/commits.html#create-a-commit-with-multiple-files-and-actions
# for actions detail

#DEBUG
print(INDEX_PATH)
# Check if file exists, and define commit action
try:
    f = p.files.get(file_path=INDEX_PATH, ref=REF_NAME)
    action='update'
except:
    action='create'

#sys.exit(0)

data = {
    'branch': REF_NAME,
    'commit_message': 'Generate new snippets index, {d}'.format(d=date.today()),
    'actions': [
        {
            'action': action,
            'file_path': INDEX_PATH,
            'content': index_str
        }
    ]
}

commit = p.commits.create(data)
