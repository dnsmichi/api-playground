#!/usr/bin/env python

import gitlab
import os
import sys

# DISCLAIMER: This draft does not work with the 'build_coverage_regex' not exposed via API anymore.

# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#getting-started-with-the-api
SERVER='https://gitlab.com'
GROUP_NAME='gitlab-de'
GROUP_ID=10087220 # Use the group ID for faster access on GitLab.com
#PROJECT_ID=17255377
PROJECT_ID=None
CREATE_MR=False # Toggle to create a MR suggesting .gitlab-ci.yml update 

def fetch_and_suggest(gl, project_id):
    project = gl.projects.get(project_id)

    print(project.attributes)
    if 'build_coverage_regex' in project.attributes.keys():
        print(project.attributes.build_coverage_regex)

        # Note: Must be patched with /regex/ pattern, coverage: '/\d+.\d+% of statements/'
        b_c_regex = "/" + project.attributes.build_coverage_regex + "/"

        default_branch = project.default_branch
        ci_file = '.gitlab-ci.yml' # TODO: Figure out if overridden in settings 

        print("Found Build Coverage Regex '%s' in project settings. Migration suggestions below." % (b_c_regex))

        # Make a suggestion snippet for .gitlab-ci.yml, with using a global default job
        # https://docs.gitlab.com/ee/ci/yaml/#default
        # TODO: Does not work ... need to find the job where to add the attribute onto
        ci_config = """
# Add 'coverage' attribute to CI/CD job
coverage-job:
  coverage: '{0}'

        """.format(b_c_regex)

        print("Suggested %s file config change: \n\n %s" % (ci_file, ci_config))

        print("Attempting to patch .gitlab-ci.yml in the default branch %s" % (default_branch))

        # Fetch the content of the existing .gitlab-xi.yml (will create the file if empty)
        f = project.files.get(file_path='.gitlab-ci.yml', ref=default_branch)

        # TODO: Figure out which job to patch? Maybe checking for 'cover' as string in the script sections or job name?
        new_f = ci_config + f

        # DEBUG
        print(new_f)

        # TODO: Ask for permissions
        if not CREATE_MR:
            print("CREATE_MR not enabled, stopping MR creation.")
            return

        # Create a new branch based on the default branch
        # https://python-gitlab.readthedocs.io/en/v3.4.0/gl_objects/branches.html
        branch_name = "ci-coverage-regex-15-0"
        mr_title = 'Add Test Coverage report regex into .gitlab-ci.yml'
        project.branches.create({'branch': branch_name, 'ref': default_branch})

        # Commit the change
        # https://python-gitlab.readthedocs.io/en/v3.4.0/gl_objects/commits.html
        data = {
			'branch': branch_name,
			'commit_message': mr_title,
			'actions': [
				{
					'action': 'update',
					'file_path': ci_file,
					'content': new_f
				},
			]
		}

        project.commits.create(data)

        # Create a merge request
		# https://python-gitlab.readthedocs.io/en/v3.4.0/gl_objects/merge_requests.html
        project.mergerequests.create({'source_branch': branch_name,
                                   'target_branch': default_branch,
                                   'title': mr_title})




if __name__ == "__main__":
    # Prefer keyset pagination
    # https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination
    gl = gitlab.Gitlab(SERVER, private_token=os.environ['GITLAB_TOKEN'], pagination="keyset", order_by="id", per_page=100)

    # Direct project ID
    if PROJECT_ID:
        fetch_and_suggest(gl, PROJECT_ID)
        sys.exit(0)


    # Groups and projects inside
    if GROUP_ID:
        group = gl.groups.get(GROUP_ID)

        for p in group.projects.list(include_subgroups=True, all=True):
            fetch_and_suggest(gl, p.id)


sys.exit(0)





    # TODO: Search branches which contain the .gitlab-ci.yml
    #branches = full_p.branches.list()





