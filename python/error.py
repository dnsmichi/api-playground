
#!/usr/bin/env python

import gitlab

# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#getting-started-with-the-api
SERVER='https://gitlab.com'
USER_NAME='everyonecancontribute'

# Session is not authenticated
gl = gitlab.Gitlab(SERVER)

print(gl.users.status())
