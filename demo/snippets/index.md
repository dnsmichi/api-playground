# Snippets Overview
_Ordered by due date._


## 2022-07-22 

- [2022-07-21 Notes](2022-07-21.md) (Release Upgrade)

## 2022-08-01 

- [2022-07-19 Notes](2022-07-19.md) (DevOps Learn)

## 2022-09-01 

- [2022-07-20 Notes](2022-07-20.md) (SRE Observability Talk)
